// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyCzRMEvvNXw6BJdKk6NzzNuyl0faZz-dGA",
    authDomain: "carrito-5cac3.firebaseapp.com",
    projectId: "carrito-5cac3",
    storageBucket: "carrito-5cac3.appspot.com",
    messagingSenderId: "322453182717",
    appId: "1:322453182717:web:5ae023aafff57176760694",
    measurementId: "G-B4LXQYVB7R"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
