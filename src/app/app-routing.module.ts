import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { environment } from '../environments/environment';
import { GuardService } from './guards/guard.service';
import { PageErrorComponent } from './shared/page-error/page-error.component';
import { ProducstModule } from './producst/producst.module';
const routes: Routes = [
  {
    path: 'auth',
    loadChildren : ()=> import('./login/login.module').then(m => m.LoginModule)

  },
  {
    path: '404',
    component : PageErrorComponent
  },
  {
    path: 'home',
    canActivate : [GuardService],
    loadChildren : ()=> import('./home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'product',
    canActivate : [GuardService],
    loadChildren : ()=> import('./producst/producst.module').then(m => m.ProducstModule)
  },
  {
    path: 'cart',
    canActivate : [GuardService],
    loadChildren : ()=> import('./cart/cart.module').then(m => m.CartModule)
  },
  {
    path: '**',
    redirectTo: 'auth'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
