import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from './pages/product/product.component';
import { AllProductsComponent } from './components/all-products/all-products.component';

const route : Routes = [
  {
    path: '',
    children: [
      {
        path: 'product',
        component: ProductComponent
      },
      {
        path: 'all',
        component: AllProductsComponent
      }
    ]
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(route)
  ]
})
export class ProductsRoutingModule { }
