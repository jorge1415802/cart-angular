import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.css']
})
export class AllProductsComponent implements OnInit {

  products : any[] = [];
  carts : any[] = [];
  display : boolean = false;
  product : any;
  cart : any;

  quantityForm : FormGroup = this.fb.group({
    quantity : ['',Validators.required]
  });

  constructor(private productService : ProductService, private fb : FormBuilder) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.productService.getProducts().subscribe(res => {
      console.log(res);
      res.forEach((data : any) => {
        this.products.push({
          id : data.payload.doc.id,
          ...data.payload.doc.data()
        })
      })
      console.log(this.products)
    })
  }

  add(product : any) {
    this.display = true;
    this.product = product;
    this.productService.getCart().subscribe(res => {
      res.forEach((data : any) => {
        this.carts.push({
          id : data.payload.doc.id,
          ...data.payload.doc.data()
        })
      })
      
    })
  }

  sendQuantity() {
    console.log(this.carts)
    if(this.quantityForm.valid) {
      this.carts.map(cart => {
        if(cart.status != "pending") {
          let dataCart = cart;
          this.cart = cart;
          this.productService.addProductoToCard(this.product,dataCart,this.quantityForm.get('quantity')?.value)
          .then(() => console.log("ok"))
          .catch((err) => console.log(err))
          this.productService.setCart(cart);
        }
      })
      this.display = false;
    }
    else {
      console.log("invalido")
    }
  }

}
