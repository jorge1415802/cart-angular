import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './pages/product/product.component';
import { RouterModule } from '@angular/router';
import { ProductsRoutingModule } from './products-routing.module';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AllProductsComponent } from './components/all-products/all-products.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [ProductComponent, AllProductsComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    ProductsRoutingModule,
    AngularMaterialModule,
    RouterModule,
    ReactiveFormsModule
  ],
})
export class ProducstModule { }
