import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore'
@Injectable({
  providedIn: 'root'
})
export class ProductService {


  _cart : any;
  constructor(public firestore : AngularFirestore) { }


  get cart() {
    return this._cart;
  }

  setCart(cart : any) {
    this._cart = cart;
  }

  getProducts() {
    return this.firestore.collection('products').snapshotChanges();
  }

  addProductoToCard(product : any,cart : any,quantity : number) {
    const data = {
      cart_id : cart.id,
      product_id : product.id,
      quantity
    }
    return this.firestore.collection('products_carts').add(data);
  }

  createCart() {
    const data = {
      status : "pending"
    };
    return this.firestore.collection('carts').add(data);
  }

  getCart() {
    return this.firestore.collection('carts').snapshotChanges();
  }
  
}
