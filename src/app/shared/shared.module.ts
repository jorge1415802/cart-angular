import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { NavComponent } from './nav/nav.component';



@NgModule({
  declarations: [NavComponent],
  imports: [
    CommonModule,
    AngularMaterialModule
  ],
  exports:[NavComponent]
})
export class SharedModule { }
