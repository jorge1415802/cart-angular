import {ActionReducerMap } from '@ngrx/store';
import * as products from './store/reducer/products.reducer';

export interface AppState {
    products : products.State
}

export const appReducer : ActionReducerMap<AppState> = {
    products : products.productReducer
}