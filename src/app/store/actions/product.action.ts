import { createAction, props } from "@ngrx/store";

export const setStateProducts = createAction(
    '[Products] Set State Products',
    props<{products : any[]}>()
);