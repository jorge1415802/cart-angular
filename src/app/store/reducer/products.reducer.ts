import { createReducer, on } from "@ngrx/store";
import { setStateProducts } from "../actions/product.action";


const initialState : State = {
    products : [],
}


export interface State {
    products : any[];
}

const _productReducer = createReducer(
    initialState,
    on(setStateProducts,(state,{products})=> {
        return {
            ...state,
            products : products
        }
    })
);

export function productReducer(state : any, action :any) {
    return _productReducer(state, action);
}