import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { pipe, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(public firestore  : AngularFirestore) { }

  getProduct_Cart() {
    return this.firestore.collection('products_carts').snapshotChanges();
  }

  getCartById(id : string) {
    return this.firestore.collection('products_carts').doc(id).snapshotChanges();
  }

  getProductById(id : string) {
    return this.firestore.collection('products').doc(id).snapshotChanges();
  }

  deleteProductFromCart(id : string) {
    return this.firestore.collection('products_carts').doc(id).delete()
  }

  updateCart(id : string, cart : any) {
    return this.firestore.collection('products_carts').doc(id).update(cart);
  }
}
