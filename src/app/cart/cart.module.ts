import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './pages/cart/cart.component';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { CartRoutingModule } from './cart-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [CartComponent],
  imports: [
    CommonModule,
    AngularMaterialModule,
    CartRoutingModule,
    FlexLayoutModule,
    RouterModule,
    ReactiveFormsModule
  ]
})
export class CartModule { }
