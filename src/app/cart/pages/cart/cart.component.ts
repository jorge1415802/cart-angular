import { SelectionModel } from '@angular/cdk/collections';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { CartService } from '../../services/cart.service';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  data : any[] = [];
  dataCart : any[] = [];
  dataProduct : any[] = [];

  cart : any;

  isPressed : boolean = false;

  updateForm : FormGroup = this.fb.group({
    quantity : ['',Validators.required]
  })
    
  constructor(private cartService : CartService, private fb : FormBuilder) { }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.cartService.getProduct_Cart().subscribe(res =>{
      res.forEach((data : any) => {
        this.data.push({
          id : data.payload.doc.id,
          ...data.payload.doc.data()
        })
      })
      this.data.forEach((element : any)=> {
        this.cartService.getCartById(element.cart_id).subscribe(res2 => {
          this.dataCart.push(res2);
        })
        this.cartService.getProductById(element.product_id).subscribe(res3 => {
          this.dataProduct.push(res3);
        })
      })
    })
  }

  delete(cart : any) {
    this.cartService.deleteProductFromCart(cart.id)
    .then(()=> {
      console.log("ok")
    })
    .catch((error) => console.log(error))
  }

  save(cart : any) {
    this.cart = cart;
    console.log(cart)
    this.isPressed = true;
  }

  update() {
    console.log(this.cart)
    if(this.updateForm.valid) {
      const data = {
        id : this.cart.id,
        product_id : this.cart.product_id,
        cart_id : this.cart.cart_id,
        quantity : this.updateForm.get('quantity')?.value
      }
      this.cartService.updateCart(this.cart.id,data)
        .then(()=> {
          console.log("ok")
        })
        .catch((err)=> console.log(err))
      this.isPressed = false;
    }
    
  }

}
