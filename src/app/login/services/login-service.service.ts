import { Injectable } from '@angular/core';
import { first } from 'rxjs/operators'
import { AngularFirestore } from '@angular/fire/firestore'
import { AngularFireAuth } from '@angular/fire/auth'
@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  log : boolean = false;

  constructor(public fAuth : AngularFireAuth) { }

  get logged() {
    return this.log;
  }

  setLogin(log : boolean) {
    this.log = log;
  }

  async register(email : string, pass : string) {
    try {
      const result = await this.fAuth.createUserWithEmailAndPassword(email,pass);
      return result;  
    } catch (error) {
      return console.log(error)
    }
    
  }

  async login(email : string,pass : string) {
    try {
      const result = await this.fAuth.signInWithEmailAndPassword(email,pass);
      return result;  
    } catch (error) {
      return console.log(error)
    }    
  }

  async logout() {
    try {
      await this.fAuth.signOut();  
    } catch (error) {
      return console.log(error)
    }
    
  }

  getCurrentUser() {
    return this.fAuth.authState.pipe(first()).toPromise();
  }
}
