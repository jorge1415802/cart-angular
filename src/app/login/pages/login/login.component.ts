import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginServiceService } from '../../services/login-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup = this.fb.group({
    email : ['',Validators.required],
    pass : ['',Validators.required]
  })

  
  constructor(private fb : FormBuilder, private loginService : LoginServiceService,private router : Router) { }

  ngOnInit(): void {
  }

  singIn() {
    if(this.loginForm.valid) {
      const {email,pass} = this.loginForm.value;
      this.loginService.login(email,pass).then(async()=> {
        const user = await this.loginService.getCurrentUser();
        console.log(user);
        this.loginService.setLogin(true);
        this.router.navigate(['/home/home'])
      }).catch((error)=> {
        console.log(error)
      })
    }
    else {
      console.log("formulario invalido")
    } 
  }

}
