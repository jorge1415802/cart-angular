import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginServiceService } from '../../services/login-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm : FormGroup = this.fb.group({
    email : ['',Validators.required],
    pass : ['',Validators.required]
  })
  
  constructor(private registerService : LoginServiceService, private fb : FormBuilder) { }

  ngOnInit(): void {
  }

  register() {
    if(this.registerForm.valid) {
      const {email, pass} =  this.registerForm.value;
      this.registerService.register(email,pass);
    }
    else {
      console.log("formulario invalido")
    }
  }

}
